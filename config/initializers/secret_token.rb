# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Nootz::Application.config.secret_key_base = 'e8bf396e608a0580241c7828731c2b72021f04e58b992e2500542157e0e181e5c5537a682ffacc2043ebcafa21d9fc156895ee2e2f90b0d2ba78c0e90b4e6d13'
