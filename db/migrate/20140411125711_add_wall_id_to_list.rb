class AddWallIdToList < ActiveRecord::Migration
  def change
    add_column :lists, :wall_id, :integer
  end
end
