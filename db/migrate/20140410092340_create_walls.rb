class CreateWalls < ActiveRecord::Migration
  def change
    create_table :walls do |t|
      t.string :title
      t.integer :user_id

      t.timestamps
    end
  end
end
