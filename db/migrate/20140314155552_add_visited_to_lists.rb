class AddVisitedToLists < ActiveRecord::Migration
  def change
	add_column :lists, :visited, :boolean
  end
end
