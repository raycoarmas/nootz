class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.string :title
      t.date :limit_date

      t.timestamps
    end
  end
end
