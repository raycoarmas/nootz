class AddWallIdToNote < ActiveRecord::Migration
  def change
    add_column :notes, :wall_id, :integer
  end
end
