class AddVisitedToNotes < ActiveRecord::Migration
  def change
	add_column :notes, :visited, :boolean
  end
end
