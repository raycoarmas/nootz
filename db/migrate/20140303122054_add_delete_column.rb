class AddDeleteColumn < ActiveRecord::Migration
  def change
    add_column :notes, :deleted, :boolean, default: false
    add_column :lists, :deleted, :boolean, default: false
  end
end
