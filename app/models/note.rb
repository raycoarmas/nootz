class Note < ActiveRecord::Base
	scope :end_today, -> { where("limit_date = ? and visited = ?",DateTime.now.to_date,false)}
	scope :notDeleted, -> { where(deleted: false) }
	scope :deleted, -> { where(deleted: true) }	
end
