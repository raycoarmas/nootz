require 'digest'
class User < ActiveRecord::Base 
  attr_accessor :password
  validates :email, uniqueness: { case_sensitive: false }, format: {with: /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/i, message: "Formato de correo no válido"}

  validates :password, length: {in: 4..20}, confirmation: true, presence: {if: :password_required?}
  validates :password_confirmation, presence: true

	has_many :walls
  

  before_save :encrypt_new_password
  after_save :build_profile

  def build_profile
    Profile.create(user: self)
  end

  def self.authenticate(email,password)
    user = find_by_email(email)
    return user if user && user.authenticated?(password)
  end

  def authenticated?(password)
    self.hashed_password == encrypt(password)
  end

  protected
    def encrypt_new_password
      return if password.blank?
      self.hashed_password = encrypt(password)
    end
    
    def password_required?
      hashed_password.blank? || password.present?
    end

    def encrypt(string)
      Digest::SHA1.hexdigest(string)
    end  

end
