json.array!(@lists) do |list|
  json.extract! list, :id, :title, :limit_date
  json.url list_url(list, format: :json)
end
