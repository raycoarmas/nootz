class NotesController < ApplicationController
  before_action :set_note, only: [:show, :edit, :update, :destroy,:visit]
def nuevanota
	@note = Note.create(title:'Nueva nota', deleted:false, content: 'Contenido de la nota', visited:false, limit_date:Date.today, wall_id: current_wall.id);
	respond_to do |format|
		 format.js
	end
end
def mostrarnota
	@note = Note.find_by_id(params[:id])
	respond_to do |format|
		 format.js
	end
end
  # GET /notes
  # GET /notes.json
  # El índice de notas se compone de las notas que no han sido marcadas como eliminadas
  def index
    @notes = Note.notDeleted
  end

  # GET /notes/1
  # GET /notes/1.json
  def show
  end

  def visit
   @note.visited = true
   if @note.save
           respond_to do |format|
		 format.js
	end
      end
  end

  # GET /notes/new
  def new
    @note = Note.new
  end

  # GET /notes/1/edit
  def edit
  end

  # POST /notes
  # POST /notes.json
  def create
    @note = Note.new(note_params)
    @note.limit_date = Time.now # MODO DEVELOPMENT
    @note.visited = false
    respond_to do |format|
      if @note.save
        format.html { redirect_to @note, notice: 'Note was successfully created.' }
        format.json { render action: 'show', status: :created, location: @note }
      else
        format.html { render action: 'new' }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notes/1
  # PATCH/PUT /notes/1.json
  def update
    respond_to do |format|
      if @note.update(note_params)
        format.html { redirect_to @note, notice: 'Note was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notes/1
  # DELETE /notes/1.json
  def destroy
    #@note.destroy	A partir de ahora se marca como deleted en lugar de ser eliminados
    @note.update_attribute(:deleted, true)
    respond_to do |format|
      format.html { redirect_to notes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_note
      @note = Note.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def note_params
      params.require(:note).permit(:title, :content, :limit_date, :wall_id)
    end
end
