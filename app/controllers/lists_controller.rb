class ListsController < ApplicationController
  before_action :set_list, only: [:show, :edit, :update, :destroy,:visit]
def nuevalista
	@list = List.create(title:'Nueva lista', deleted:false, visited:false, limit_date:Date.today,wall_id: current_wall.id);
	respond_to do |format|
		 format.js
	end
end
def mostrarlista
	@list = List.find_by_id(params[:id])
	respond_to do |format|
		 format.js
	end
end

  # GET /lists
  # GET /lists.json
  def index
    @lists = List.notDeleted
  end

  # GET /lists/1
  # GET /lists/1.json
  def show
	
  end

  def visit
  @list.visited = true
   if @list.save
           respond_to do |format|
		 format.js
	end
      end
  end

  # GET /lists/new
  def new
    @list = List.new
  end

  # GET /lists/1/edit
  def edit
  end

  # POST /lists
  # POST /lists.json
  def create
    @list.limit_date = Time.now # MODO DEVELOPMENT
    @list.visited = false
    respond_to do |format|
      if @list.save
        format.html { redirect_to @list, notice: 'List was successfully created.' }
        format.json { render action: 'show', status: :created, location: @list }
      else
        format.html { render action: 'new' }
        format.json { render json: @list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lists/1
  # PATCH/PUT /lists/1.json
  def update
    respond_to do |format|
      if @list.update(list_params)
        format.html { redirect_to @list, notice: 'List was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lists/1
  # DELETE /lists/1.json
  def destroy
    #@list.destroy	A partir de ahora se marca como deleted en lugar de ser eliminados
    @list.update_attribute(:deleted, true)
    respond_to do |format|
      format.html { redirect_to lists_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_list
      @list = List.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def list_params
      params.require(:list).permit(:title, :limit_date, :wall_id)
    end
end
