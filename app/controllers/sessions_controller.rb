class SessionsController < ApplicationController
  def new
    
  end  

  def create
    if !params.has_key?(:email) && !params.has_key?(:password)
	render :new
    else
	    if user= User.authenticate(params[:email],params[:password])
	      	session[:user_id]=user.id
		redirect_to walls_path
	    else
	      flash.now[:notice] = "El email o la contraseña son erróneos."
	      render :new    
	    end
    end
  end

  def destroy
	reset_session
        redirect_to root_path
  end

end
