class WallsController < ApplicationController
  before_action :set_wall, only: [:show, :edit, :update, :destroy]

def nuevomuro
	@wall = Wall.create(title:'Nuevo muro', user_id: current_user.id);
	respond_to do |format|
		 format.js
	end
end
def mostrarmuro
	@wall = Wall.find_by_id(params[:id])
	respond_to do |format|
		 format.js
	end
end
  # GET /walls
  # GET /walls.json
  def index
    @walls = Wall.all
  end

  # GET /walls/1
  # GET /walls/1.json
  def show
    session[:wall_id]=@wall.id
  end

  # GET /walls/new
  def new
    @wall = Wall.new
  end

  # GET /walls/1/edit
  def edit
  end

  # POST /walls
  # POST /walls.json
  def create
    @wall = Wall.new(wall_params)

    respond_to do |format|
      if @wall.save
        format.html { redirect_to @wall, notice: 'Wall was successfully created.' }
        format.json { render action: 'show', status: :created, location: @wall }
      else
        format.html { render action: 'new' }
        format.json { render json: @wall.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /walls/1
  # PATCH/PUT /walls/1.json
  def update
    respond_to do |format|
      if @wall.update(wall_params)
        format.html { redirect_to @wall, notice: 'Wall was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @wall.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /walls/1
  # DELETE /walls/1.json
  def destroy
    @wall.notes.each do |note|
        note.deleted=true
        note.save
    end
    @wall.lists.each do |list|
        list.deleted=true
        list.save
    end
    @wall.destroy
    respond_to do |format|
      format.html { redirect_to walls_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_wall
      @wall = Wall.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def wall_params
      params.require(:wall).permit(:title, :user_id)
    end
end
